# Managing Function Inputs

- `bind`
  - presets the `this` context
  - partially applies arguments
- **currying**
  - a function that expects multiple arguments is broken down into successive
    chained functions that each take a single argument (arity: 1) and return
    another function to accept the next argument
- Aim to remove unnecessary parameter-argument mapping
  - **tacit programming**
  - point-free style
