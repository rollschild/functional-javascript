import { compose } from "./composition.js";
import { curry } from "./curry.js";

function isLongEnough(str) {
  return str.length >= 5;
}
function isShortEnough(str) {
  return str.length <= 10;
}

function strUppercase(str) {
  return str.toUpperCase();
}
function strConcat(str1, str2) {
  return str1 + str2;
}

function strUppercaseReducer(list, str) {
  return [...list, strUppercase(str)];
}

/* function isLongEnoughReducer(list, str) {
 *   if (isLongEnough(str)) {
 *     return [...list, str];
 *   }
 *   return list;
 * } */
/* function isShortEnoughReducer(list, str) {
 *   if (isShortEnough(str)) {
 *     return [...list, str];
 *   }
 *   return list;
 * } */

function filterReducer(predicateFn, combinerFn) {
  return function reducer(list, val) {
    if (predicateFn(val)) {
      return combinerFn(list, val);
    }
    return list;
  };
}

function mapReducer(mapperFn, combinerFn) {
  return function reducer(list, val) {
    return combinerFn(list, mapperFn(val));
  };
}

const curriedFilterReducer = curry(function filterReducer(
  predicateFn,
  combinerFn,
) {
  return function reducer(list, val) {
    if (predicateFn(val)) {
      return combinerFn(list, val);
    }
    return list;
  };
});

const curriedMapReducer = curry(function mapReducer(mapperFn, combinerFn) {
  return function reducer(list, val) {
    return combinerFn(list, mapperFn(val));
  };
});

const isLongEnoughReducer = curriedFilterReducer(isLongEnough)(listCombine);
const isShortEnoughReducer = curriedFilterReducer(isShortEnough)(listCombine);
const strToUppercaseReducer = curriedMapReducer(strUppercase)(listCombine);

function listCombine(list, val) {
  return [...list, val];
}

const x = curriedMapReducer(strUppercase);
const y = curriedFilterReducer(isLongEnough);
const z = curriedFilterReducer(isShortEnough);

const shortEnoughReducer = z(listCombine);
const longAndShortEnoughReducer = y(shortEnoughReducer);

// x(y(z(listCombine)))
// const upperLongAndShortEnoughReducer = x(longAndShortEnoughReducer);

/*
 * `composition` is a **transducer**
 * a composed function expecting a combination function to make a reducer
 */
const composition = compose(
  curriedMapReducer(strUppercase),
  curriedFilterReducer(isLongEnough),
  curriedFilterReducer(isShortEnough),
);
const upperLongAndShortEnoughReducer = composition(listCombine);
const words = ["kobe", "michael", "lebron", "AI", "Ja", "abcdefghijklmn"];

function transduce(transducer, combinerFn, initialValue, list) {
  const reducer = transducer(combinerFn);
  return list.reduce(reducer, initialValue);
}
