# Monad

- a **monad** is a way of composing functions that require context in addition to the return value, such as computation, branching, or effects
- A **monad** is a data structure, a type, a set of behaviors that are specifically designed to make working with a type
  predictable
- Functions can compose: `a => b => c` becomes `a => c`
- Functors can compose functions with context:
  - given `F(a)` and two functions, `a => b => c`, return `F(c)`
- Monads can compose _type lifting_ functions:
  - `a => M(b), b => M(c)` becomes `a => M(c)`
- **type lift** means to lift a type into a context, wrapping values inside a data type that supplies the computational context `a => M(a)`
- **Flatten** can be used to unwrap an extra layer of context that might be added by applying a type lifting function using a functor map operation
- **FlatMap** is the operation that defines a monad

## What Monads Are Made Of

- A monad is based on a simple symmetry
  - `of`: type lift `a => M(a)`
  - `map`:
    - the application of a function `a => M(b)` inside the monad context, which yields `M(M(b))`
  - `flatten`: the unwrapping of one layer of monadic context
    - `M(M(b)) => M(b)`
  - **FlatMap**: map + flatten
    - `f(a).flatMap(g) = M(c)`
- **Just**
  - a basic primitive monad
  - _all_ monad instances will have `map(..)`, `chain(..)` (a.k.a. `bind(..)` or `flatMap(..)`), and `ap(..)` methods
  - `chain(..)` flattens the monad
- `Maybe`
  - pairing of two monads: `Just` and `Nothing`
  - the power of `Maybe` abstraction is to encapsulate that behavior/no-op duality implicitly
- `MaybeHumble`
  - a factory function that produces a `Maybe` monad instance
- A monad is how you organize behavior around a value in a more declarative way

## The Monad Laws

### Left and Right Idendity Laws

```
of then f === f then of
```

### Associativity Law

```
(f then g) then h === f then (g then h)
of(x).flatMap(f).flatMap(g) === of(x).flatMap((x) => f(x).flatMap(g))
```

## Monad Examples in JavaScript

- Promise
- Observable
