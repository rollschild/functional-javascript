# OOP

- Essence of OOP
  - **encapsulating state**
  - **decoupling objects from each other**
  - **runtime adaptability**
- Three different forms of object composition
  - **aggregation**
  - **concatenation**
  - **delegation**
- Delegation
  - object forwards or **delegates** to another object
  - JavaScript's built-in types use delegation to forward built-in method calls up the prototype chain
  - `[].map()` delegates to `Array.prototype.map()`
  - `obj.hasOwnProperty()` delegates to `Object.prototype.hasOwnProperty()`
