function reverseArgs(fn) {
  return function argsReversed(...args) {
    return fn(...args.reverse());
  }
}

function partialRight(fn, ...presetArgs) {
  return function partiallyApplied(...laterArgs) {
    return fn(...laterArgs, ...presetArgs);
  }
}

function foo(x, y, ...others) {
  console.log(x, y, others);
}

let f = partialRight(foo, "try this arg");
f(0, 1, 2, 3);
