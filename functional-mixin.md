# Functional Mixins

- **Mixins** are a form of object composition, where component features get mixed intot a composite object so that properties of each mixin become properties of the composite object
- `Object.getOwnPropertyDescriptors()`
- Using the `class` provided by a library is sometimes practical, if the library:
  - does **NOT** require you to extend your own classes, and
  - does **NOT** require you to directly use the `new` keyword - the framework handles the instantiation for you
-
