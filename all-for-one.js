function unary(fn) {
  // closure
  return function onlyOneArg(arg) {
    return fn(arg);
  };
}
