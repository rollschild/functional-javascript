const t = (value) => {
  const add = (n) => t(value + n);

  return Object.assign(add, {
    toString: () => `t(${value})`,
    valueOf: () => value,
  });
};
// const x = 20;
// console.log(t(x)(t(0)).toString());
// console.log(t(x)(t(0)).toString() === t(x).toString());

const pipe =
  (...fns) =>
  (x) =>
    fns.reduce((y, f) => f(y), x);
const add = (...fns) => pipe(...fns)(t(0));
const sum = add(t(2), t(4), t(-1));
console.log("sum:", sum.toString());
