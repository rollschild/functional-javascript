# Closures vs. Object

- Closures and objects actually have identical mutation behavior
  - because what we care about is **value** mutability
- **Isomorphic**
  - refers to the code that can be used/shared in both server and browser
  - isomorphism is a special case of bijective (aka 2-way) morphism that
    requires not only that the mapping must be able to go in either
    direction, but also that the behavior is identical in either form
- **Closures and objects are isomorphic representations of state (and its
  associated functionality)**
- Conceptually, the structure of a closure is not mutable
  - you can never add to or remove state from a closure
- The advantage of managing state as public properties on an object is that
  it's easier to enumerate (and iterate) all the data in the state
  -
