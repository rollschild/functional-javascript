function isPrime(num, divisor = 2) {
  if (num < 2 || (num > 2 && num % divisor === 0)) {
    return false;
  }

  if (divisor <= Math.sqrt(num)) {
    return isPrime(num, divisor + 1);
  }

  return true;
}
console.log(isPrime(15));
console.log(isPrime(17));
console.log(isPrime(2));
