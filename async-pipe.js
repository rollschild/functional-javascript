const asyncPipe =
  (...fns) =>
  (x) =>
    fns.reduce(async (y, f) => f(await y), x);

const readUser = () => Promise.resolve(true);
const getFolderInfo = () => Promise.resolve(true);
const uploadToFolder = () => Promise.resolve("Success!");

const uploadFiles = asyncPipe(readUser, getFolderInfo, uploadToFolder);
const log = (thing) => console.log(thing);
const user = "rollschild";
const folder = "home";
const files = ["a", "b", "c"];
uploadFiles({ user, folder, files }).then(log); // Success!
