# List Operations

- `Array.prototype.entires()`
- **Functors**
  - A functor is a value that has a utility for using an operator function on
    that value, which preserves composition
- A combination/reduction is abstractly defined as taking two values and making
  them into one value
- `flatMap(..)` or `chain(..)` funtions
  - map-then-flatten
- `zip(..)`
- `merge(..)`
  - merge two lists by interleaving values from each source
  - can be composed from `flatten` and `zip`
  - can also be implemented using `mergeReducer`
- the method chain style - a.k.a. **fluent API** style
- Composing method chains

  - the array methods receive the implicit `this` argument
  - we need a more `this` aware version of `partial(..)`
  - we also need a version of `compose(..)` that calls each of the partially
    applied methods in the context of the chain
    - the input value it's being passed (via implicit `this`) from the previous
      step

- Adapting standalones to methods - the fluent chain style - two options:
  1. Extend the built-in `Array.prototype` with additional methods
  2. Adapt a standalone utility to work as a reducer function and pass it to
     the `reduce(..)` instance method
  - Do **NOT** do option 1.
  - It's **never** a good idea to extend built-in natives like
    `Array.prototype`
