const view = (lens, store) => lens.view(store);
const set = (lens, value, store) => lens.set(value, store);

const lensProp = (prop) => ({
  view: (store) => store[prop],
  set: (value, store) => ({
    ...store,
    [prop]: value,
  }),
});

const fooStore = {
  a: "foo",
  b: "bar",
};

const aLens = lensProp("a");
const bLens = lensProp("b");

const a = view(aLens, fooStore);
const b = view(bLens, fooStore);
// console.log("a, b", a, b);

const bazStore = set(aLens, "baz", fooStore);
// console.log(view(aLens, bazStore));

// Over
const over = (lens, f, store) => set(lens, f(view(lens, store)), store);
const uppercase = (x) => x.toUpperCase();

console.log(over(aLens, uppercase, bazStore));
