function partialProps(fn, presetArgsObj) {
  return function partiallyApplied(laterArgsObj) {
    return fn(Object.assign({}, presetArgsObj, laterArgsObj));
  };
}

function curryProps(fn, arity = 1) {
  return (function nextCurried(prevArgsObj) {
    return function curried(nextArgObj) {
      let allArgsObj = Object.assign({}, prevArgsObj, nextArgObj);

      if (Object.keys(allArgsObj).length >= arity) return fn(allArgsObj);
      else return nextCurried(allArgsObj);
    }
  })({});
}

function foo({x, y, z} = {}) {
  console.log(`x: ${x}, y: ${y}, z: ${z}`);
}

let partialFn = partialProps(foo, {y: 2037});
let curryFn = curryProps(foo, 3);
partialFn({z: -1, x: 111});
curryFn({y: 2037})({z: -1})({x: 111});
