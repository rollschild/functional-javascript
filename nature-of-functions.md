# The Nature of Functions

- functions are the center of FP
- **morphism**
  - a set of values that maps to another set of values
- Function vs. Procedure
  - a procedure is an arbitrary collection of functionality
  - a function takes input(s) and _always_ has a `return` value
- _All_ your functions should take input(s) and return output(s)
- Arguments are the values you pass in
- Parameters are the named variables inside the function that receive those
  passed-in values
- **arity**
  - number of parameters in a function declaration
  - function with arity of 1 is also called **unary**
  - a function with arity of 2 is also called **binary**
- `Function.length`
  - number of parameters expected by the function
  - excludes the rest parameter
  - stops before the first parameter with a default value
- avoid using `arguments` wherever possible
- **NEVER** access arguments positionally, like `arguments[1]`
- declarative code communicates more effectively than imperative code
- **named arguments**
- Prefer explicit patterns over implicit ones
- side effects and purity
  - side effects: the implicit function output
  - pure function: a function that has no side effects
- **closure**
  - closure is when a function remembers and accesses variables from outside of
    its own scope, even when that function is executed in a different scope
- You should name your functions!
  - stack trace debugging
  - reliable self-reference
  - readability
- `this`
  - the `this` keyword of a function is automatically bound per function call
  - provides an object context for the function to run against
  - `this` is actually an implicit parameter input for the function
  -
