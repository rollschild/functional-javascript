# Value Immutability

- values of primitive types (`number`, `string`, `boolean`, `null`, and `undefined`) are already immutable
- But JS has "**boxing**"
  - JS automatically wraps `number`, `string`, and `boolean` in its object counterpart:
    - `Number`
    - `String`
    - `Boolean`
- Value Immutability:
  - when we need to change the state in the program, we must create and track a new value rather than mutate an existing
    value
- Non-primitive values are held **by reference**
  - when passed as arguments, it's the reference that's copied, not the value itself
  - if passed as arguments, non-primitive values become **non-local**
- `const` creates a block scoped variable
- **equational reasoning**???
- We should care about **whether the values get mutated**, instead of whether they get reassigned
  - because values are portable, but lexical assignments are not
- `Object.freeze()`
  - goes through all properties/indices of an object/array and marks them read-only
  - also marks the properties non-reconfigurable
  - marks the object/array itself non-extensible
  - makes **ONLY** the top level of the object immutable
  - if you want a deeply immutable value, you need to walk the entire object/array manually and apply `Object.freeze()`
    to each of them
- We should treat _all_ received values as immutable
- You should **NEVER** use such methods on an array that is not already local to the function that's being worked on
  - `splice()`
  - `pop()`
  - `push()`
  - `shift()`
  - `unshift()`
  - `reverse()`
  - `sort()`
  - `fill()`
-  
