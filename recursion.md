# Recursion

## Definition

- recursion trades (much of) the explicit tracking of state with implicit state
  on the call stack
- Typically, recursion is most useful when the problem requires conditional
  branching and backtracking

## Tail Calls

- if a call from function `baz()` to function `bar()` happens at the very end
  of function `baz()`'s execution, referred to as a **tail call**, the stack
  frame of `baz()` isn't needed anymore
  - either the memory can be reclaimed
  - or simply reused to handle function `bar()`'s execution
- tail call recursion can run in `O(1)` fixed memory usage
- **Tail Call Optimizations (TCO)**

## Practical Tail Calls

- ES6 mandates recognition of tail calls
  - of a form called **Proper Tail Calls (PTC)**
  - with guarantee that the code in PTC form will run without unbounded stack
    memory
- A proper tail call looks like this:

```javascript
return foo(..);
```

- the function call is the last thing to execute in the surrounding function,
  and whatever value it returns is explicitly `return`ed

## Continuation Passing Style (CPS)

- **continuation** is often used to mean a function callback that specifies the
  next step(s) to execute after a certain function finishes its work

## Trampolines

- Another technique for alleviating memory pressure
- CPS-like continuations are created
  - but instead of passed in, they are shallowly returned
