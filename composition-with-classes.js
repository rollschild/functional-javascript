/*
 * This will **NOT** work for Promises
 * It's not safe to assume that you can use the `new` keyword for any factory function
 */
/* const empty = ({ constructor } = {}) =>
 *   constructor ? new constructor() : undefined; */

/*
 * `.of()` - a static method on any factory or constructor
 * it's a factory that returns a new instance of the data type containing whatever you pass into `.of()`
 */
const empty = ({ constructor } = {}) =>
  constructor.of ? constructor.of() : undefined;

const foo = Promise.resolve(42);
console.log("empty foo:", empty(foo));

const createUser = ({
  userName = "Anonymous",
  avatar = "profile.png",
} = {}) => ({
  userName,
  avatar,
  constructor: createUser,
});
createUser.of = createUser;

const emptyUser = ({ constructor } = {}) =>
  constructor.of ? constructor.of() : undefined;
const user = createUser({ userName: "guangchu", avatar: "me.png" });
console.log("user:", user);
console.log("empty user:", emptyUser(user));
console.log(
  "user.constructor === createUser.of?",
  user.constructor === createUser.of,
);

// make `.constructor` non-enumerable
const createUserV2 = ({
  userName = "Anonymous",
  avatar = "profile.png",
} = {}) => ({
  __proto__: {
    constructor: createUserV2,
  },
  userName,
  avatar,
});
