function map(mapperFn, arr) {
  const list = [];

  for (const [index, value] of arr.entries()) {
    // notice the order of value/element, index, array
    list.push(mapperFn(value, index, arr));
  }

  return list;
}
console.log(map((ele) => ele * 2, [12, 6, -1, 100]));
