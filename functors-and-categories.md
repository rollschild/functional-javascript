# Functors and Categories

- a **functor data type** is something you can map over
- `functor.map = Functor(a) ~> (a => b) => Functor(b)`
-
