function pipe(...fns) {
  return function piped(val) {
    let fnList = [...fns];

    while (fnList.length > 0) {
      val = fnList.shift()(val);
    }

    return val;
  };
}

const pipeV2 =
  (...fns) =>
  (x) =>
    fns.reduce((y, f) => f(y), x);

export default Object.freeze(pipe);
