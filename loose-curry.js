function looseCurry(fn, arity = fn.length) {
  return (function nextCurried(prevArgs) {
    return function curried(...nextArgs) {
      let args = [...prevArgs, ...nextArgs];
      if (args.length >= arity) return fn(...args);
      else return nextCurried(args);
    };
  })([]);
}

function sum(...nums) {
  let total = 0;
  for (let num of nums) {
    total += num;
  }
  return total;
}

let curriedSum = looseCurry(sum, 6);
console.log(curriedSum(1)(2, 3)(4)(5, 6));
console.log(curriedSum(1, 2, 3, 4)(5, 6));
