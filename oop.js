const objs = [{ a: "a", b: "ab" }, { b: "b" }, { c: "c", b: "cb" }];
const collection = (a, e) => a.concat([e]);
const a = objs.reduce(collection, []);
console.log("a:", a);
const concatenate = (a, o) => ({ ...a, ...o });
const c = objs.reduce(concatenate, {});
console.log("c:", c);
