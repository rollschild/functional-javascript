const {compose, composeRecursive} = require('./composition');
const {partialRight} = require('./partial');
const {pipe} = require('./pipe');

function words(str) {
  return String(str)
    .toLowerCase()
    .split(/\s|\b/)
    .filter(function examine(word) {
      return /^[\w]+$/.test(word);
    });
}

function unique(words) {
  let wordsList = [];

  for (let word of words) {
    if (wordsList.indexOf(word) === -1) wordsList.push(word);
  }

  return wordsList;
}

let text =
  'To compose two functions together, pass the  output of the first function call as the input of the  second function call.';
/*
console.log(
  compose(
    unique,
    words,
  )(text),
);
*/
function skipShorterWords(words) {
  let filteredWords = [];

  for (let word of words) {
    if (word.length > 4) filteredWords.push(word);
  }

  return filteredWords;
}

function skipLongerWords(words) {
  let filteredWords = [];

  for (let word of words) {
    if (word.length <= 4) filteredWords.push(word);
  }

  return filteredWords;
}
/*
console.log(
  compose(
    skipShorterWords,
    unique,
    words,
  )(text),
);
*/
let filterWords = partialRight(composeRecursive, unique, words);
let biggerWords = filterWords(skipShorterWords);
let shorterWords = filterWords(skipLongerWords);
console.log(biggerWords(text));
console.log(shorterWords(text));
let biggerWordsPipe = pipe(words, unique, skipShorterWords);
let shorterWordsPipe = pipe(words, unique, skipLongerWords);
console.log(biggerWordsPipe(text));
console.log(shorterWordsPipe(text));
