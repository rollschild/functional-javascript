let nums = [];
let smallCount = 0;
let largeCount = 0;

function generateRandoms(count) {
  for (let i = 0; i < count; ++i) {
    let num = Math.random();

    if (num < 0.5) {
      ++smallCount;
    } else {
      ++largeCount;
    }

    nums.push(num);
  }
}

function saferGenRandoms(count, initial) {
  let orig = {
    nums,
    smallCount,
    largeCount,
  };

  nums = initial.nums;
  smallCount = initial.smallCount;
  largeCount = initial.largeCount;

  generateRandoms(count);

  let sides = {
    nums,
    smallCount,
    largeCount,
  };

  nums = orig.nums;
  smallCount = orig.smallCount;
  largeCount = orig.largeCount;

  return sides;
}

let initialStates = {
  nums: [0.2037, 0.99, 0.6],
  smallCount: 1,
  largeCount: 2,
};

console.log(saferGenRandoms(5, initialStates));

console.log(nums);
console.log(smallCount);
console.log(largeCount);
