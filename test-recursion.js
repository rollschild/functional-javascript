function foo(x) {
  if (x < 5) return x;
  return foo(x / 2);
}

const sum = (num1, num2, ...nums) => {
  console.log(num1, num2);
  if (num1 === undefined) return 0;
  if (num2 === undefined) return num1;
  num1 = num1 + num2;
  if (nums.length === 0) return num1;
  return sum(num1, ...nums);
};
console.log(sum(6, 5, 7, 9));
