const withConstructor = (constructor) => (o) => ({
  // create the delegate [[Prototype]]
  __proto__: {
    // add the constructor prop to the new [[Prototype]]
    constructor,
  },
  ...o,
});

const pipe =
  (...fns) =>
  (x) =>
    fns.reduce((y, f) => f(y), x);
