const formatter = (formatFn) => {
  return (str) => {
    return formatFn(str);
  }
}

const lower = formatter((v) => {
  return v.toLowerCase();
})

const toUpperFirst = formatter((v) => {
  return v[0].toUpperCase() + v.substring(1).toLowerCase();
})

console.log(lower("Heyyy WHAt's uPPP..."));
console.log(toUpperFirst("Heyyy WHAt's uPPP..."));
