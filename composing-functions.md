# Composing functions

- most typical FP libraries define their `compose(..)` to work right-to-left in
  terms of ordering
- DRY: don't repeat yourself
  - have only one definition in a program for any give task
