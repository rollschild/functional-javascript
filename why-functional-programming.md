# Why Functional Programming

- **Imperative** instructs the computer _how_ to do something
- **Declarative** code focuses on describing the _what_ outcome
- Patterns in FP:
  - filtering
  - reduction
  - transducing
  - composition
  -
