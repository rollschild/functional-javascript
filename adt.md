# Abstract Data Types

- **Abstract Data Type**
  - an abstract concept defined by axioms that represent some data and operations on that data
  - common examples:
    - stack
    - list
    - set
-
