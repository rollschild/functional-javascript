import { curry } from "./curry.js";

function Just(val) {
  return { map, chain, ap, inspect };

  function map(fn) {
    return Just(fn(val));
  }

  function chain(fn) {
    return fn(val);
  }

  function ap(anotherMonad) {
    return anotherMonad.map(val);
  }

  function inspect() {
    return `Just(${val})`;
  }
}

const Maybe = { Just, Nothing, of: Just };
function Nothing() {
  return { map: Nothing, chain: Nothing, ap: Nothing, inspect };

  function inspect() {
    return "Nothing";
  }
}

function isEmpty(val) {
  return val === null || val === undefined;
}
const safeProp = curry(function safeProp(prop, obj) {
  if (isEmpty(obj[prop])) {
    return Maybe.Nothing();
  }
  return Maybe.of(obj[prop]);
});

function MaybeHumble(egoLevel) {
  return !(Number(egoLevel) >= 42) ? Maybe.of(egoLevel) : Maybe.Nothing();
}

function intro() {
  console.log("Self-intro");
}

const egoChange = curry(function egoChange(amount, concept, egoLevel) {
  console.log(`${amount > 0 ? "Learned" : "Shared"} ${concept}.`);
  return MaybeHumble(egoLevel + amount);
});

const Monad = (value) => ({
  flatMap: (f) => f(value),
  map(f) {
    return this.flatMap((a) => Monad.of(f(a)));
  },
});
Monad.of = (x) => Monad(x);

console.log(
  Monad(21)
    .map((x) => x * 2)
    .map((x) => console.log(x)),
);

/*
 * Kleisli composition
 */
const composeM =
  (method) =>
  (...ms) =>
    ms.reduce((f, g) => (x) => g(x)[method](f));

const composePromises = (...ms) => ms.reduce((f, g) => (x) => g(x).then(f));
