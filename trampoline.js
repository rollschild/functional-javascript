function trampoline(fn) {
  return function trampolined(...args) {
    let result = fn(...args);

    while (typeof result === "function") {
      result = result();
    }

    return result;
  };
}

const sum = trampoline(function sum(num1, num2, ...nums) {
  if (num1 === undefined) return 0;
  if (num2 === undefined) return num1;
  num1 += num2;
  if (nums.length === 0) {
    return num1;
  }
  return () => sum(num1, ...nums);
});

const xs = [];
for (let i = 0; i < 20000; i++) {
  xs.push(i);
}
console.log(sum(...xs));
