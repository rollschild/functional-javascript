/*
 * generator (iterator) is iterable object, which has the `@@iterator` method
 * generators can only be iterated once
 * the `@@iterator` function of a generator returns `this`
 */
/*
 * iterables that can be iterated multiple times must return a new generator on
 * every invocation of `@@iterator`
 */

function* makeIterator() {
  yield 1;
  yield 2;
  yield 3;
}

const it = makeIterator();
// for (const item of it) {
//   console.log("item:", item);
// }

console.log(it[Symbol.iterator]() === it); // true

const customIterable = {
  *[Symbol.iterator]() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
  },
};

// for (const item of customIterable) {
//   console.log("item:", item);
// }

const spread = [..."abc"];
// console.log(Array.isArray(spread), spread);

function* gen() {
  yield* ["a", "b", "c"];
}
const gened = gen();
console.log(gened.next());
console.log(gened.next());
console.log(gened.next());
console.log(gened.next()); // {value: undefined, done: true}
