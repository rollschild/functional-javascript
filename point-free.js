function paritalRight(fn, ...prevArgs) {
  return function partialApplied(...laterArgs) {
    return fn(...laterArgs, ...prevArgs);
  };
}

function uncurry(fn) {
  return function uncurried(...args) {
    let ret = fn;
    for (let arg of args) {
      ret = ret(arg);
    }

    return ret;
  };
}

function output(msg) {
  console.log(msg);
}

export function isShortEnough(str) {
  return str.length <= 5;
}

function complement(predicate) {
  return function negate(...args) {
    return !predicate(...args);
  };
}

function when(predicate, fn) {
  return function conditional(...args) {
    if (predicate(...args)) {
      return fn(...args);
    }
  };
}

export let isLongEnough = complement(isShortEnough);

let printIf = uncurry(paritalRight(when, output));

let msgOne = "Hey..";
let msgTwo = msgOne + " world...";

// printIf(isShortEnough, msgOne);
// printIf(isShortEnough, msgTwo);
//
// printIf(isLongEnough, msgOne);
// printIf(isLongEnough, msgTwo);
