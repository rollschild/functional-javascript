function identity(val) {
  return val;
}

let words = "   Michael Jordan... is the greatest of all time ... ";

console.log(words.split(/\s|\b/).filter(identity));
/*
 * JS coerces each value into true or false
 */

function output(msg, formatFn = identity) {
  msg = formatFn(msg);
  console.log(msg);
}

function upper(msg) {
  return msg.toUpperCase();
}

output("Hello, world...");
output("Hello, world...", upper);
