function gatherArgs(fn) {
  return function gatheredFn(...argsArr) {
    return fn(argsArr);
  }
}

function combine([arg1, arg2]) {
  return arg1 + arg2;
}

console.log([1, 2, 3, 4, 5].reduce(gatherArgs(combine)));
/*
 * we know reduce() function invokes a callback
 * function with two individual args.
 */
