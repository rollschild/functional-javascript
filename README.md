# Functional JavaScript

Notes and code snippets from the books:

- _Functional-Light JavaScript_ by Kyle Simpson, and
- _Composing Software_ by Eric Elliott
