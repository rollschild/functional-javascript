function rememberNumsImpure(nums) {
  return function caller(fn) {
    return fn(nums);
  };
}

function rememberNumsPure(nums) {
  nums = [...nums];
  return function caller(fn) {
    // console.log(nums);
    return fn(nums);
  };
}

function rememberNumsEvenPurer(...nums) {
  // nums = [...nums];
  return function caller(fn) {
    return fn([...nums]);
  };
}

let list = [1, 2, 3, 4, 5];

let simpleListImpure = rememberNumsImpure(list);
let simpleListPure = rememberNumsPure(list);
let simpleListEvenPurer = rememberNumsEvenPurer(...list);

function median(nums) {
  return (nums[0] + nums[nums.length - 1]) / 2;
}

console.log(simpleListImpure(median)); // 3
console.log(simpleListPure(median)); // 3
console.log(simpleListEvenPurer(median));

list.push(6);

console.log(simpleListImpure(median)); // 3.5
console.log(simpleListPure(median)); // 3
console.log(simpleListEvenPurer(median));

function firstVal(nums) {
  return nums[0];
}

function lastVal(nums) {
  return firstVal(nums.reverse());
}
/*
console.log(simpleListPure(lastVal)); // 5
console.log(list); // NOT changed
console.log(simpleListPure(lastVal)); // 1
*/
console.log(simpleListEvenPurer(lastVal));
console.log(list);
console.log(simpleListEvenPurer(lastVal));
