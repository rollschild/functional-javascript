// `...` gathers parameters
export function partial(fn, ...presetArgs) {
  return function partiallyApplied(...laterArgs) {
    return fn(...presetArgs, ...laterArgs);
  };
}

export function partialRight(fn, ...presetArgs) {
  return function partiallyApplied(...laterArgs) {
    return fn(...laterArgs, ...presetArgs);
  };
}

function add(x, y) {
  return x + y;
}
/*
console.log(
  [1, 2, 3, 4, 5].map(function adder(val) {
    return add(3, val);
  }),
);
*/
/* Another way */
/*
console.log([1, 2, 3, 4, 5].map(partial(add, 3)));
*/
function foo(x, y, z, ...restArgs) {
  console.log(x, y, z, restArgs);
}

let func = partialRight(foo, "last..");
