# Reducing Side Effects

- when a function makes a reference to a variable outside itself, this is called a **free variable**
- outputs and changes in state are the most common side effects
- also **side causes**
- randomness is a side cause
  - usually start with a starting point
  - the sequence will always be predictable with the same starting point
  - called **seed**
- We have to treat built-in random number generation as a side cause
- I/O
  - input is a side cause
  - output is a side effect
- Idempotence
  - Mathematical idempotence:
    - `foo(foo(x)) === foo(foo(foo(x)))`
  - Programming idempotence:
    - `f(x)` results in the same program behavior as `f(x); f(x);`
    - HTTP operations such as `PUT`
- Purity:
  - a function with no side effects/causes is a **pure** function
  - a pure function _can_ reference free variables, as long as those free variables aren't side causes
- the unary function is pure:
  - it's not possible to affect the callability of a function value

```javascript
function unary(fn) {
  return functiononlyOneArg(arg) {
    return fn(arg);
  };
}
```

- A pure function has **referential transparency**
- **Memoization**
  - the performance optimization side effecting
  - by hiding the caching of results so they **cannot** be observed by any other part of the program
- the purity of a function is judged from the outside, regardless of what goes on inside 
- Doing deep copy vs. doing shallow copy
