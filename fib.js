// function fib_(n) {
//   if (n === 1) {
//     return 1;
//   }
//   return fib(n - 2);
// }
// function fib(n) {
//   if (n === 0) {
//     return 0;
//   }
//   return fib(n - 1) + fib_(n);
// }
// console.log(fib(4));

function fib(n, cont = identity) {
  if (n <= 1) return cont(n);
  return fib(n - 2, (n2) => fib(n - 1, (n1) => cont(n1 + n2)));
}
