import pipe from "./pipe.js";

const binary = (fn) => (arg1, arg2) => fn(arg1, arg2);

const pipeReducer = binary(pipe);

const arr = [3, 17, 6, 4];
const fn = arr.map((v) => (n) => v * n).reduce(pipeReducer);

const isOdd = (v) => v % 2 === 1;
arr.reduce((list, v) => (isOdd(v) ? list.push(v) : undefined, list), []);
