import { compose } from "./composition.js";
import { curry, sum } from "./curry.js";

const unique = (arr) => arr.filter((v, idx) => arr.indexOf(v) === idx);

const flatten = (arr) =>
  arr.reduce((list, v) => list.concat(Array.isArray(v) ? flatten(v) : v), []);

const flattenWithDepth = (arr, depth = Infinity) =>
  arr.reduce((list, v) =>
    list.concat(
      depth > 0
        ? depth > 1 && Array.isArray(v)
          ? flattenWithDepth(v, depth - 1)
          : v
        : /*
           * Here we concat `[v]`, NOT `v`, to maintain the nested structure
           */
          [v],
    ),
  );

/*
 * Notice the order of the parameters mapperFn and arr
 */
/*
 * This implementation has worse performance
 * because it processes the list twice
 */
// const flatMap = (mapperFn, arr) => flattenWithDepth(arr.map(mapperFn), 1);

// Better performance?
const flatMap = (mapperFn, arr) =>
  arr.reduce((list, v) => list.concat(mapperFn(v)), []);

const zip = (arr1, arr2) => {
  const zipped = [];
  const arrOne = [...arr1];
  const arrTwo = [...arr2];

  while (arrOne.length > 0 && arrTwo.length > 0) {
    zipped.push([arrOne.shift(), arrTwo.shift()]);
  }

  return zipped;
};

const mergeLists = (arr1, arr2) => {
  const merged = [];
  const arrOne = [...arr1];
  const arrTwo = [...arr2];

  while (arrOne.length > 0 || arrTwo.length > 0) {
    if (arrOne.length > 0) {
      merged.push(arrOne.shift());
    }
    if (arrTwo.length > 0) {
      merged.push(arrTwo.shift());
    }
  }

  return merged;
};

const mergeReducer = (merged, v, idx) => (merged.splice(idx * 2, 0, v), merged);

const partialThis = (fn, ...presetArgs) =>
  /*
   * Intentionally returns a `function`
   */
  function partiallyAppied(...laterArgs) {
    return fn.apply(this, [...presetArgs, ...laterArgs]);
  };

const composeChainedMethods =
  (...fns) =>
  (result) =>
    fns.reduceRight((result, fn) => fn.call(result), result);

const filter = curry((predicateFn, arr) => arr.filter(predicateFn));
const map = curry((mapperFn, arr) => arr.map(mapperFn));
const reduce = curry((reducerFn, initialValue, arr) =>
  arr.reduce(reducerFn, initialValue),
);

/*
 * Adapting methods to standalones
 */
const unboundMethod = (methodName, argCount = 2) =>
  curry((...args) => {
    const obj = args.pop();
    return obj[methodName](...args);
  });

function flattenReducer(list, v) {
  return list.concat(Array.isArray(v) ? v.reduce(flattenReducer, []) : v);
}

const guard = (fn) => (arg) => arg != undefined ? fn(arg) : arg;

const BinaryTree = (value, parent, left, right) => ({
  value,
  parent,
  left,
  right,
});
BinaryTree.forEach = function forEach(visitFn, node) {
  if (node) {
    if (node.left) {
      forEach(visitFn, node.left);
    }

    visitFn(node);

    if (node.right) {
      forEach(visitFn, node.right);
    }
  }
};

BinaryTree.map = function map(mapperFn, node) {
  if (node) {
    const newNode = mapperFn(node);
    // newNode.parent = node.parent;
    if (!node.parent) {
      newNode.parent = undefined;
    }
    newNode.left = node.left ? map(mapperFn, node.left) : undefined;
    newNode.right = node.right ? map(mapperFn, node.right) : undefined;

    if (newNode.left) {
      newNode.left.parent = newNode;
    }
    if (newNode.right) {
      newNode.right.parent = newNode;
    }

    return newNode;
  }
};

const banana = BinaryTree("banana");
const apple = (banana.left = BinaryTree("apple", banana));
const cherry = (banana.right = BinaryTree("cherry", banana));
const apricot = (apple.right = BinaryTree("apricot", apple));
const avocado = (apricot.right = BinaryTree("avocado", apricot));
const cantaloupe = (cherry.left = BinaryTree("cantaloupe", cherry));
const cucumber = (cherry.right = BinaryTree("cucumber", cherry));
const grape = (cucumber.right = BinaryTree("grape", cucumber));

const BANANA = BinaryTree.map(
  (node) => BinaryTree(node.value.toUpperCase()),
  banana,
);
// BinaryTree.forEach((node) => console.log(node.value), BANANA);
// BinaryTree.forEach((node) => console.log(node.value), banana);

BinaryTree.reduce = function reduce(...args) {
  let [reducerFn, initialValue, node] = args;
  if (args.length < 3) {
    node = initialValue;
  }

  if (node) {
    let result;

    if (args.length < 3) {
      // no current node
      if (node.left) {
        result = reduce(reducerFn, node.left);
      } else {
        return node.right ? reduce(reducerFn, node, node.right) : node;
      }
    } else {
      result = node.left
        ? reduce(reducerFn, initialValue, node.left)
        : initialValue;
    }

    result = reducerFn(result, node);
    result = node.right ? reduce(reducerFn, result, node.right) : result;
    return result;
  }

  return initialValue;
};
// console.log(
//   BinaryTree.reduce((result, node) => [...result, node.value], [], banana),
// );

BinaryTree.filter = function filter(predicateFn, node) {
  if (node) {
    let newNode;
    let newLeft = node.left ? filter(predicateFn, node.left) : undefined;
    let newRight = node.right ? filter(predicateFn, node.right) : undefined;

    if (predicateFn(node)) {
      // include this node
      newNode = BinaryTree(node.value, node.parent, newLeft, newRight);
      if (newLeft) {
        newLeft.parent = newNode;
      }
      if (newRight) {
        newRight.parent = newNode;
      }
    } else {
      if (newLeft) {
        if (newRight) {
          newNode = BinaryTree(undefined, node.parent, newLeft, newRight);
          newLeft.parent = newRight.parent = newNode;

          if (newRight.left) {
            let minRightNode = newRight;
            while (minRightNode.left) {
              minRightNode = minRightNode.left;
            }

            newNode.value = minRightNode.value;

            if (minRightNode.right) {
              minRightNode.parent.left = minRightNode.right;
              minRightNode.right.parent = minRightNode.parent;
            } else {
              minRightNode.parent.left = undefined;
            }

            minRightNode.right = minRightNode.parent = undefined;
          } else {
            newNode.value = newRight.value;
            newNode.right = newRight.right;
            if (newRight.right) {
              newRight.right.parent = newNode;
            }
          }
        } else {
          return newLeft;
        }
      } else {
        return newRight;
      }
    }

    return newNode;
  }
};

const vegies = [
  "asparagus",
  "avocado",
  "borccoli",
  "carrot",
  "celery",
  "corn",
  "cucumber",
  "lettuce",
  "potato",
  "squash",
  "zucchini",
];
const whatToBuy = BinaryTree.filter(
  (node) => vegies.indexOf(node.value) > -1,
  banana,
);
console.log(
  BinaryTree.reduce((result, node) => [...result, node.value], [], whatToBuy),
);
