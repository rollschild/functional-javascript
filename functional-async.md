# Functional Async

- The most complicated state in your entire application is **time**
- It's the coordination of the responses to these _async_ actions, each of which
  has the potential to change the state of the application, that requires much extra effort
- a **Promise** represents a single (future) value in a time-independent manner
- Extracting the value from a promise is the asynchronous form of the
  synchronous assignment (via `=`) of an immediate value
- A promise spreads an `=` assignment operation over time, but in a trustable
  (time-independent) fashion

## Reactive FP

- **Functional Reactive Programming**
- _producer_ and _listener_

## Observables

- in RxJS universe, an **Observer** subscribes to an **Observable**
-
