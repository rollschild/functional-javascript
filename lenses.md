# Lenses

- a **lens** is a composable pair of _pure_ getter and setter functions which focus on a particular field inside an object

```javascript
const getX = ([x]) => x;
const getY = ([x, y]) => y;
const getZ = ([x, y, z]) => z;
```

```javascript
const setY =
  ([x, _, z]) =>
  (y) =>
    [x, y, z];
```

- Rules
  - `view(lens, set(lens, value, store)) === value`
  - `set(lens, b, set(lens, a, store)) === set(lens, b, store)`
  - `set(lens, view(lens, store), store) === store`
-
