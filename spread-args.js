function foo(x, y) {
  console.log(x + y);
}

function bar(fn) {
  fn([2037, -1]);
}

function spreadArgs(fn) {
  return function spreadFn(argsArr) {
    return fn(...argsArr);
  }
}

bar(spreadArgs(foo));
/* spreadArgs(foo)([2037, -]);
 * foo(...[2037, -1]);
 */
