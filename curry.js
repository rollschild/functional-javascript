export function curry(fn, arity = fn.length) {
  return (function nextCurried(prevArgs) {
    return function curried(nextArg) {
      let args = [...prevArgs, nextArg];

      if (args.length >= arity) return fn(...args);
      else return nextCurried(args);
    };
  })([]);
}

function add(x, y) {
  return x + y;
}

// One good example
let adder = curry(add);
// ...that you know ahead that function add() is to be adapted,
// ...but the value 5 isn't known yet.
// console.log("adder string:", adder.toString());
// console.log("adder's length:", adder.length);

/*
 * Another example
 */
export function sum(...nums) {
  let total = 0;
  for (let num of nums) {
    total += num;
  }
  return total;
}

/* vs. */
let curriedSum = curry(sum, 5);

const curryV2 =
  (f, arr = []) =>
  (...args) =>
    ((a) => (a.length === f.length ? f(...a) : curryV2(f, a)))([
      ...arr,
      ...args,
    ]);
const add3 = curryV2((a, b, c) => a + b + c);
// console.log("add3", add3.toString());
// console.log(add3(1, 2, 3));
// console.log(add3(1, 2)(3));
