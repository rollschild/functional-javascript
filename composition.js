function compose2(outerFn, innerFn) {
  return function composed(origVal) {
    return outerFn(innerFn(origVal));
  };
}

function compose1(...fns) {
  return function composed(val) {
    let fnList = [...fns];

    while (fnList.length > 0) {
      val = fnList.pop()(val);
    }

    return val;
  };
}

function makeAdder(x) {
  return function add(y) {
    return x + y;
  };
}

function subtractByOne(x) {
  return x - 1;
}
function multiplyByTwo(x) {
  return x * 2;
}
function multiplyByThree(x) {
  return x * 3;
}
function add(x, y) {
  return x + y;
}

// can also use reduceRight
function compose3(...fns) {
  return function reduced(val) {
    return [...fns].reverse().reduce(function reducer(val, fn) {
      return fn(val);
    }, val);
  };
}

// it runs the `reduce` looping once, up front at composition time
// and defers all function call calculations
// a.k.a. lazy calculation
// function compose(...fns) {
//   return [...fns].reverse().reduce(function reducer(fn1, fn2) {
//     return function composed(...args) {
//       return fn2(fn1(...args));
//     };
//   });
// }

export function compose(...fns) {
  return function composed(result) {
    return fns.reduceRight(function (result, fn) {
      return fn(result);
    }, result);
  };
}

const calculator = compose(multiplyByThree, multiplyByTwo, subtractByOne, add);

/*
 * structure of recursion
 * composeRecursive(composeRecursive(fn1, fn2, ...), fnN)
 */
function composeRecursive(...fns) {
  let [fn1, fn2, ...restFns] = [...fns].reverse();

  let composedFn = function composed(...args) {
    return fn2(fn1(...args));
  };

  if (restFns.length === 0) return composedFn;
  else return composeRecursive(...restFns.reverse(), composedFn);
}

// module.exports = { compose2, compose1, compose, compose3, composeRecursive };
