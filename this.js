function sum() {
  return this.x + this.y;
}

let context = {
  x: 2037,
  y: -1,
}

console.log(sum.call(context));

let s = sum.bind(context);
console.log(s());
