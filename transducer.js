const compose =
  (...fns) =>
  (x) =>
    fns.reduceRight((y, f) => f(y), x);
const map =
  (f) =>
  (step) =>
  (a = step(), c) =>
    step(a, f(c));
const filter = (predicate) => (step) => (a, c) => predicate(c) ? step(a, c) : a;

// usage
const double = (x) => {
  console.log("double called:", x);
  return x * 2;
};
const doubleMap = map(double);
const step = (a, c) => console.log(c);

doubleMap(step)(0, 4);

console.log("============================================");

const isEven = (n) => {
  console.log("isEven called:", n);
  return n % 2 === 0;
};

// `isEven` is called _before_ `double`
const doubleEvens = compose(filter(isEven), map(double));

const arrayConcat = (a, c) => {
  console.log("arrayConcat concat called:", c);
  return a.concat([c]);
};

const xform = doubleEvens(arrayConcat);

const result = [1, 2, 3, 4, 5, 6].reduce(xform, []);
console.log("result:", result);

// A simplified version of `reduced`
const reduced = (v) => ({
  get isReduced() {
    return true;
  },
  valueOf: () => v,
  toString: () => `Reduced(${JSON.stringify(v)})`,
});

const curry =
  (f, arr = []) =>
  (...args) =>
    ((a) => (a.length === f.length ? f(...a) : curry(f, a)))([...arr, ...args]);

// a **foldable** is any object that supplies a `.reduce()` method
const transduce = curry((step, initial, xform, foldable) =>
  foldable.reduce(xform(step), initial),
);

const concatArray = (a, c) => a.concat([c]);
const toArray = transduce(concatArray, []);

const result2 = toArray(doubleEvens, [1, 2, 3, 4, 5, 6]);
console.log("result2:", result2);

// Non-naive implementation of `map` Transducer
/* const mapV2 = (f) => (next) =>
 *   transducer({
 *     init: () => next.init(),
 *     result: (a) => next.result(a),
 *     step: (a, c) => next.step(a, f(c)),
 *   }); */
